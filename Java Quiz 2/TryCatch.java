public class TryCatch {
    /*
    //  Try and Catch example code
    //  When executing Java code, different errors can occur:
    //  coding errors made by the programmer, errors due to wrong input, or other unforeseeable things.

    //  When an error occurs, Java will normally stop and generate an error message.
    //  The technical term for this is: Java will throw an exception (throw an error).

    //  The try statement allows us to define a block of code to be tested for errors
    //  while it is being executed.

    //  The catch statement allows us to define a block of code to be executed,
    //  if an error occurs in the try block.
    */
    public static void main(String[] args) {
        /*
        //  If an error occurs, we can use try...catch
        //  to catch the error and execute some code to handle it
         */
        try {
            int[] myNumbers = {1, 2, 3};
            //  This will generate an error, because myNumbers[10] does not exist
            System.out.println(myNumbers[10]);
        }
        //  This way, when an error occurs, we can handle the response
        catch (Exception e) {
            System.out.println("Something went wrong.");
        }
    }
}
