package Polymorphism;

public class Animal {

    /*
    //  Superclass with method "makeSound()" which outputs "Grr..."
    //  but will change depending on object initialization
    */

    public void makeSound() {
        System.out.println("Grr...");
    }
}
