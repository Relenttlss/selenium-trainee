package Polymorphism;

public class Polymorphism {
    /*
    //  Polymorphism example code
    //  Every type of animal is a subclass of the superclass "Animal"

    //  Polymorphism is the ability of the objects of a class to offer
    //  a different and independent response depending on the parameters
    */

    public static void main(String[] args) {
        /*
        //  Initialization of Animal class as Dog and Cat, so we can
        //  check how polymorphism works
        */

        Animal a = new Dog();
        Animal b = new Cat();

        /*
        //  "makeSound()" is the method that exist in each subclass
        //  but have different uses
        */

        a.makeSound();
        //Outputs "Woof"

        b.makeSound();
        //Outputs "Meow"
    }
}
