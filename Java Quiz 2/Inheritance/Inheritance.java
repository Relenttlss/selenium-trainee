package Geometry;

public class Inheritance {
    /*
    //  Inheritance example code
    //  Every type of shape is a subclass of the superclass "Shape"

    //  Inheritance allows us to define a class based on another existing class.
    //  This is one of the bases of code reuse, rather than copy and paste.
    //  This is specified by adding the "extends" clause after the class name.
    //  In the extends clause we will indicate the name of the base class from which we want to inherit.
    //  When inheriting from a base class we will inherit both the attributes and the methods,
    //  while the constructors are used, but not inherited.
     */

    public static void main(String[] args) {

        /*
        //  The objects "Shape" are initiated with
        //  different shapes and every "calculateArea()" method
        //  belongs to inherited shape
         */

        Shape coin = new Circle("Coin");
        System.out.println("The area for the coin is: " + coin.calculateArea());
        Shape square = new Rectangle();
        System.out.println("The area of the square is: " + square.calculateArea());
        Shape triangle = new Triangle("Pyramid");
        System.out.println("The area of the pyramid is " + triangle.calculateArea());

    }
}
