package Geometry;

/*
 * The shape class is the superclass
 */
public abstract class Shape {
    private String shapeName;
    private double area;

    public Shape() {
        shapeName = "shape";
        area = 0;
    }

    public Shape(String shapeName) {
        this.shapeName = shapeName;
        area = 0;
    }

    public Shape(String name, double area) {
        this.shapeName = name;
        this.area = area;
    }

    /*
    //  Abstract class, so a subclass can define its out method
    //  for calculation area
     */
    public abstract double calculateArea();

}