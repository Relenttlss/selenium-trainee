import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SeleniumExceptions {

    private static WebDriver driver;

    @Before
    public void setUp() {
        //  Init setup for WebDriver and access to Page
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");
    }

    @Test
    public void exceptionsHandle() {
        /**
         *  1.- NoSuchElementException
         *
         *  The exception occurs when WebDriver is unable to find and locate elements.
         *  This may occur when the element locator is not well defined in snippet.
         */

        //Using Wait
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Your Id")));

        try { driver.findElement(By.id("Your Id")).click(); }
        catch (NoSuchElementException e) { e.getMessage(); }

        /**
         *  2.- NoSuchWindowException
         *
         *  This is thrown when WebDriver tries to switch to an invalid window.
         *  This can happen when the window handle does not exist or the switch is not available.
         */

        for (String handle : driver.getWindowHandles()) {
            try { driver.switchTo().window(handle); }
            catch (NoSuchWindowException e) { System.out.println(e.getMessage()); }
        }

        /**
         *  3.- NoSuchFrameException
         *
         *  When we use our WebDriver to try to switch to an invalid frame.
         */

        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("Frame Here"));

        try { driver.switchTo().frame("Frame Here"); }
        catch (WebDriverException e) { System.out.println(e.getMessage()); }

        /**
         *  4.- NoAlertPresentException
         *
         *  This occurs when WebDriver tries to switch to an alert, which is not available.
         *
         *  Always use explicit or fluent wait for a particular time in all cases where an alert is expected.
         */

        wait.until(ExpectedConditions.alertIsPresent());

        try { driver.switchTo().alert().accept(); }
        catch (NoAlertPresentException e) { System.out.println(e.getMessage()); }

        /**
         *  5.- InvalidSelectorException
         *
         *  This occurs when a selector is incorrect or syntactically invalid.
         *  This exception occurs commonly when XPATH locator is used.
         *
         *  To avoid this, we should check the locator or the syntax.
         */

        try { driver.findElement(By.xpath("Your Xpath")).click();
        } catch (InvalidSelectorException e) { e.getMessage(); }

        /**
         *  6.- ElementNotVisibleException
         *
         *  This is a subclass of ElementNotInteractableException class.
         *  This exception is thrown when WebDriver tries to perform an action on an invisible web element.
         *
         *  We can use wait for the element to get completely visible.
         */

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("Your Id")));

        try { driver.findElement(By.id("Your Id")).click(); }
        catch (WebDriverException e) { System.out.println(e.getMessage()); }

        /**
         *  7.- ElementNotSelectableException
         *
         *  This indicates that the web element is present in the web page but cannot be selected.
         *
         *  We can add a wait command to wait until the element becomes clickable.
         *  If there is still an exception, it is caught.
         */

        wait.until(ExpectedConditions. elementToBeClickable(By.id("Your Select Id")));

        try { Select dropdown = new Select(driver.findElement(By.id("swift"))); }
        catch (WebDriverException e) { System.out.println(e.getMessage()); }

        /**
         *  8.- TimeoutException
         *
         *  This exception occurs when a command completion takes more than the wait time.
         *  Waits are mainly used in WebDriver to avoid exceptions.
         *  Sometimes test page might not load completely before next command in the program.
         *
         *  To avoid this, we can manually check the average time for a page to load and adjust the wait.
         */

        wait.until(webDriver ->
                ((JavascriptExecutor)webDriver).executeScript("return document.readyState").equals("complete"));

        driver.get("Your URL");

        /**
         *  9.- NoSuchSessionException
         *
         *  Thrown when a method is called after quitting the browser by WebDriver.quit().
         *  This exception can be reduced by using driver.quit() at the completion of all tests.
         *  Do not try to use them after each test case.
         *
         *  In this case, refer to the tearDown() method at the end of the code.
         */

        tearDown();

        /**
         *  10.- StaleElementReferenceException
         *
         *  This exception says that a web element is no longer present in the web page.
         *  This is thrown when an object for a particular web element was created in the program
         *  without any problem and however, this element is no longer present in the window.
         *
         *  We can confirm that we are trying to do the action in the correct window.
         *  To avoid issues due to DOM refresh, we can use Dynamic Xpath
         */

        try { driver.findElement(By.xpath("Relative Xpath")).sendKeys("Keys"); }
        catch (StaleElementReferenceException e) { e.getMessage(); }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
