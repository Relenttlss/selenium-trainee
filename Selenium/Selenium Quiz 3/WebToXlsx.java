import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class WebToXlsx {
    /**
     *  Selenium Quiz 3
     *  With this snippet we can test Apache POI to create an xlsx file with data obtained from the web
     *  Please consider changing the xlsx file creation path for your computer in "Path" below
     *  Please don't close the windows while running
     */

    private static WebDriver driver;
    private final String sheetName = "Countries";
    JavascriptExecutor js;
    String filePath = "C:\\Users\\User\\Desktop\\CountriesList.xlsx";
    String ssPath1 = "C:\\Users\\User\\Desktop\\Success.png";
    String ssPath2 = "C:\\Users\\User\\Desktop\\Failure.png";

    @Before
    public void setUp() {
        //  Init setup for WebDriver and access to Page
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://cosmocode.io/automation-practice-webtable/");
    }

    @Test
    public void WebToExcel() throws IOException {
        //  We instantiate XLUtily to make use of the method setCellData()
        XLUtility xlUtility = new XLUtility(filePath);
        js = (JavascriptExecutor) driver;

        //Write headers
        xlUtility.setCellData(sheetName, 0, 0, "Country");
        xlUtility.setCellData(sheetName, 0, 1, "Capital(s)");
        xlUtility.setCellData(sheetName, 0, 2, "Currency");
        xlUtility.setCellData(sheetName, 0, 3, "Primary Language(s)");

        //Capture Rows
        WebElement table = driver.findElement(By.xpath("(//table[@id='countries'])/tbody"));

        int rows = table.findElements(By.xpath("tr")).size() - 1; //Rows in web table

        //  Show an alert for two seconds to let you know what is done in the code and
        //  then create an xlsx file with all the information collected
        try {
            js.executeScript("alert('Web scrapping in process... This alert will close automatically');");
            Thread.sleep(2000);
            driver.switchTo().alert().dismiss();
            
            //Read data
            for (int r = 1; r <= rows; r++) {
                for (int c = 0; c <= 3; c++) {
                    String data = table.findElement(By.xpath("tr[" + (r + 1) + "]/td[" + (c + 2) + "]")).getText();
                    //Write data in Sheet
                    //System.out.println("Row "+ r+ " Column "+ c+ " Element "+ data);
                    xlUtility.setCellData(sheetName, r, c, data);
                }
            }
            js.executeScript("alert('Successfully');");
            driver.switchTo().alert();

            //Copy the file to a location and handle exception
            try {
                //  Take the screenshot
                BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                ImageIO.write(image, "png", new File(ssPath1));
                driver.switchTo().alert().dismiss();
            } catch (IOException | AWTException e) {
                System.out.println(e.getMessage());
            }
        } catch (IOException | InterruptedException  e){
            js.executeScript("alert('Unable to find Web Elements, please try again');");
            driver.switchTo().alert();

            try {
                //  Take the screenshot
                BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

                ImageIO.write(image, "png", new File(ssPath2));
                driver.switchTo().alert().dismiss();
            } catch (IOException | AWTException exception) {
                System.out.println(e.getMessage());
            }
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
