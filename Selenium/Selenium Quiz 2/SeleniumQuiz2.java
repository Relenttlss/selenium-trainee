import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class SeleniumQuiz2 {
    /*
    //  This quiz was made to run on Chrome with chromedriver.
    //  Please be sure to include the Selenium dependency in pom.xml:
        <dependencies>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-server</artifactId>
                <version>3.11.0</version>
            </dependency>
        </dependencies>
     */
    //  Initiate objs
    private static WebDriver driver;
    static WebDriverWait wait;

    @Before
    public void setUp() {
        //System properties for chromedriver
        String exePath = "C:\\Program Files\\ChromeDriver\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void Quiz2(){

        /**<-- StaleElements -->
         *  1.- What is StaleElementException in selenium
         *
         *  This means that we are referring to an old element or a no longer available element.
         *  is a WebDriver error that occurs because the referenced web element is no longer attached to the DOM.
         *  Selenium keeps a track of all elements in form of a Reference as when findElement/findElements is used
         *  and while reusing the element, selenium uses that Reference instead of finding the element again in the DOM.
         *  This could happen because the element was completely deleted or is no longer attached to the DOM.
         */

        //Handle by Refreshing the page
        driver.navigate().refersh();
        driver.findElement(By.xpath("your xpath")).click();

        //By Expected Conditions
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("yourID")));

        /**
         *
         *
         */

        /**<-- Drag and Drop -->
         *  2.- How to implement drag and drop
         *
         *  This is an action performed with a mouse when a user moves (drags) a web element and then places (drops)
         *  it into an alternate area.
         */

        String URL = "https://demoqa.com/droppable/";
        driver.get(URL);

        //Actions class method to drag and drop
        Actions builder = new Actions(driver);

        WebElement from = driver.findElement(By.id("draggable"));

        WebElement to = driver.findElement(By.id("droppable"));

        //Perform drag and drop
        builder.dragAndDrop(from, to).perform();

        //verify text changed in to 'Drop here' box
        String textTo = to.getText();

        try {
            Assert.assertEquals("Dropped!", textTo);
            System.out.println("PASS: Source is dropped to target as expected");

        } catch (ComparisonFailure failure){
            System.out.println("FAIL: Source couldn't be dropped to target as expected");
        }
        
        /**
         *
         *
         */

        /** <-- Close or Quit -->
         *  3.- What is the difference between driver.close() and .quit()
         *
         *  This methods are used to close a browser in Selenium:
         *
         *  driver.close() closes only the current window on which Selenium is running automated tests.
         *  The WebDriver session, however, remains active.
         *
         *  driver.quit() method closes all browser windows and ends the WebDriver session.
         */

        driver.close();
        driver.quit();

        /**
         *
         *
         */

        /** <-- switchTo -->
         *  4.- Explain 3 scenario to use driver.switchTo() method
         *
         *  We can use switchTo to handle multiple windows to switch between windows
         *
         *  1: Open multiple child windows and then navigate back to the final window handle.
         *  2: Open a new tab and then switch back to the last window.
         *  3: Open a new tab and change the controller to it.
         */

        //Open a new tab and then switch back to the last window to complete the other pending activities.
        WebDriver driverSwitch = new ChromeDriver();
        driverSwitch.get("https://www.google.com");

        //implicit wait
        driverSwitch.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driverSwitch.findElement(By.name("q")).sendKeys("Selenium");
        driverSwitch.findElement(By.name("btnk")).click();

        //  Window handles
        Set<String> w = driverSwitch.getWindowHandles();
        //  Window handles iterate
        Iterator<String> t = w.iterator();
        String h = t.next();
        String p = t.next();
        // switching child window
        driverSwitch.switchTo().window(h);
        // switching parent window
        driverSwitch.switchTo().window(p);
        // Open a new tab and change the controller to it
        driverSwitch.switchTo().window(WindowType.TAB);

        /**
         *
         *
         */

        /** <-- Waits -->
         *  5.- Which one is preferred, Implicit or Explicit wait?
         *
         *  While running Selenium tests, it is common for testers to get the message “Element Not Visible Exception“.
         *  This appears when a particular web element with which WebDriver has to interact, is delayed in its loading.
         *  To prevent this Exception, Selenium Wait Commands must be used.
         *
         *  Depending on the situation, it is known whether explicit or implicit wait is preferable:
         *  Implicit Wait directs the Selenium WebDriver to wait for a certain measure of time
         *  before throwing an exception.
         *
         *  By using the Explicit Wait command, the WebDriver is directed to wait until
         *  a certain condition occurs before proceeding with executing the code.
         *
         *  Implicit Wait stays in place for the entire duration for which the browser is open.
         *  Explicit Wait is more intelligent, but can only be applied for specified elements.
         */

        //Implicit Wait
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //Explicit Wait
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("yourID")));

        /**
         *  However, implicit wait increases test script execution time.
         *  It makes each command wait for the defined time before resuming test execution.
         *  If the application responds normally, the implicit wait can slow down the execution of test scripts.
         */
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
