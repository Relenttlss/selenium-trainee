import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class RegisterUser {
    /**
     * Register User Scenario
     * This java class works with "Fullname" and "Fullpassword" previously given
     * but if desired, you can assign arbitrary password and email below
     */

    private static WebDriver driver = null;
    private static final String fullemail = "FILL12asdsda3@example.com";
    private static final String fullpasword = "password$$123";
    static WebDriverWait wait;

    @BeforeMethod
    public static void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void register() {
        //  Init setup for WebDriver and access to Page
        //  the use of explicit waits is present to allow a slow connection to access
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        setUp();

        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
        wait = new WebDriverWait(driver, 20);

        //  Saving the GUI element reference into a "element" variable of WebElement type
        WebElement emailAddress = driver.findElement(By.id("email_create"));
        WebElement submitCreate = driver.findElement(By.id("SubmitCreate"));

        //  Send keys with credentials to test if valid mail format
        emailAddress.sendKeys(fullemail);
        LogInUser.fullmail = emailAddress.getText();

        //  Create Account button
        submitCreate.click();
        System.out.println("Loading Sign Up Form...");

        /*  Try Catch to wait for response while loading navigation
        //  If the email is valid, sign up form will load.
        //  Else, this will automatically try to Create Account again.
         */
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_gender1")));
            System.out.println("Passed");
        } catch (Exception e) {
            System.out.println("Email not valid, please try again");
            System.out.println("Loading...");
            register();
        }

        WebElement title = driver.findElement(By.id("id_gender1"));
        WebElement firstName = driver.findElement(By.id("customer_firstname"));
        WebElement lastName = driver.findElement(By.id("customer_lastname"));
        WebElement password = driver.findElement(By.id("passwd"));
        WebElement address = driver.findElement(By.id("address1"));
        WebElement city = driver.findElement(By.id("city"));
        WebElement zip = driver.findElement(By.id("postcode"));
        WebElement phone = driver.findElement(By.id("phone_mobile"));
        WebElement alias = driver.findElement(By.id("alias"));
        WebElement submit = driver.findElement(By.id("submitAccount"));

        //  Form fill data
        System.out.println("Filling Format...");
        title.click();
        firstName.sendKeys("FirstName");
        lastName.sendKeys("LastName");
        password.sendKeys(fullpasword);

        //Selecting the dropdown element by locating its id
        Select selectDay = new Select(driver.findElement(By.id("days")));
        Select selectMonth = new Select(driver.findElement(By.id("months")));
        Select selectYear = new Select(driver.findElement(By.id("years")));
        Select selectState = new Select(driver.findElement(By.id("id_state")));

        //Selecting the option to register Birth as 1/12/2000 and State Nevada
        selectDay.selectByValue("1");
        selectMonth.selectByValue("12");
        selectYear.selectByValue("2000");

        address.sendKeys("3799 S Las Vegas Blvd");
        city.sendKeys("Las Vegas");

        selectState.selectByValue("28");

        zip.sendKeys("89109");
        phone.sendKeys("+1 877-880-0880");
        alias.click();
        alias.clear();
        alias.sendKeys("MGM Grand Hotel");

        //  Click on Submit form and wait for response
        submit.click();
        System.out.println("Loading Sign Up...");

        /*  If form is correctly filled, this will pass as successful Sign Up
        //  and webdriver will be quited.
        //  Else, this will automatically try to Sign Up again.
         */
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("info-account")));
            System.out.println("Sign Up Successfully");
        } catch (Exception e) {
            System.out.println("Sign Up Failed, please try again");
            System.out.println("Loading...");
            register();
        } finally {
            tearDown();
        }

    }

    @AfterMethod
    public static void tearDown() {
        driver.quit();
    }
}
