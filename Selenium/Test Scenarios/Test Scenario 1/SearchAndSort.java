import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class SearchAndSort {

    /**
     * Search and Sort Scenario
     * This java class works with "itemSearch" and "Sort" previously given
     * but if desired, you can assign arbitrary item and SortBy below
     */

    //  Initiate objs
    private static WebDriver driver;
    public static String itemSearch = "dress";
    static int sort = 1;
    static WebDriverWait wait;

    @BeforeMethod
    public static void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void searchAdd() {
        //  Init setup for WebDriver and access to Page
        //  the use of explicit waits is present to allow a slow connection to access
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        setUp();

        driver.get("http://automationpractice.com/index.php");
        wait = new WebDriverWait(driver, 20);

        //  Saving the GUI element reference into a "element" variable of WebElement type
        WebElement itemType = driver.findElement(By.id("search_query_top"));
        WebElement searchButton = driver.findElement(By.name("submit_search"));

        //  Send key and submit search
        itemType.sendKeys(itemSearch);
        searchButton.click();

        /*  Try Catch to wait for response while loading search
        //  If the itemSearch is valid, results will be shown
        //  Else, this will automatically try to Search again.
         */
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.product_list.grid.row")));
            System.out.println("Search Passed");
        } catch (Exception e) {
            System.out.println("Item not valid, please try again");
            System.out.println("Loading...");
            searchAdd();
        }

        Select selectSort = new Select(driver.findElement(By.id("selectProductSort")));

        //Selecting the option to sort by, to change this just update "Sort" variable
        selectSort.selectByIndex(sort);

        /*  If item is sorted found, this will pass as successful
        //  and webdriver will be quited.
        //  Else, this will automatically try to Search again.
         */
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.product_list.grid.row")));
            System.out.println("Items sorted Successfully");
        } catch (Exception e) {
            System.out.println("Sort Failed, please try again");
            System.out.println("Loading...");
            searchAdd();
        } finally {
            tearDown();
        }

    }

    @AfterMethod
    public static void tearDown() {
        driver.quit();
    }
}
