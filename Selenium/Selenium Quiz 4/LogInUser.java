import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class LogInUser {
    /**
     * Login Scenario
     * This java class works with "Fullname" and "Fullpassword" previously given
     * but if desired, you can assign arbitrary password and email below
     */

    //  Initiate objs
    private static WebDriver driver;
    private String fullmail;
    private String fullpassword;
    static WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        //  Init setup for WebDriver and access to Page
        //  the use of explicit waits is present to allow a slow connection to access
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //Path of the excel file
        File file = new File("C:\\Users\\User\\IdeaProjects\\YE Examples\\AutomationPractice\\TestScenarios1.xlsx");
        FileInputStream fs = new FileInputStream(file);
        //Creating a workbook
        XSSFWorkbook workbook = new XSSFWorkbook(fs);
        XSSFSheet sheet = workbook.getSheet("Scenario1");
        Row row1 = sheet.getRow(7);
        Row row2 = sheet.getRow(8);
        Cell id = row1.getCell(1);
        Cell pass = row2.getCell(1);
        fullmail = id.getStringCellValue();
        fullpassword = pass.getStringCellValue();
    }

    @Test
    public void login() {
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
        wait = new WebDriverWait(driver, 20);

        //  Saving the GUI element reference into a "element" variable of WebElement type
        WebElement username = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.id("passwd"));
        WebElement login = driver.findElement(By.name("SubmitLogin"));

        //  Send keys with credentials
        username.sendKeys(fullmail);
        password.sendKeys(fullpassword);

        //  Log in button
        login.click();
        System.out.println("Logging...");

        /*  Try Catch to wait for response while loading navigation
        //  If the credentials are validated, log in will pass as successful
        //  and webdriver will be quited.
        //  Else, this will automatically try to Log In again.
         */
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("info-account")));
            System.out.println("Log In Successfully");
        } catch (Exception e) {
            System.out.println("Log In Failed, please try again");
            System.out.println("Loading...");
            login();
        } finally {
            tearDown();
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
