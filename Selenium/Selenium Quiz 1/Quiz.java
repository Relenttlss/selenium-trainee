import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Quiz {
    /*
    //  This quiz was made to run on Chrome with chromedriver.
    //  Please be sure to include the Selenium dependency in pom.xml:
        <dependencies>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-server</artifactId>
                <version>3.11.0</version>
            </dependency>
        </dependencies>
     */

    public static void main(String[] args) throws InterruptedException {
        //System properties for chromedriver
        String exePath = "C:\\Program Files\\ChromeDriver\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);

        /**<-- LOCATORS -->
         * 1.- What are the different type element locators of Selenium?
         *
         * Selenium supports 8 different types of locators namely id, name, className, tagName, linkText, partialLinkText,
         * CSS selector and xpath.
         * Id is one of the most reliable and fast methods of element recognition.
         * CSS selector and XPath can identify dynamic elements on a web page.
         *
         */

        //Creating the object for chrome driver
        WebDriver driverLocators = new ChromeDriver();
        driverLocators.get("http:\\www.demoqa.com\\");

        //Locate by ID Attribute
        driverLocators.get("https://www.demoqa.com/automation-practice-form");
        driverLocators.findElement(By.id("firstName"));

        //Locate by Name attribute
        driverLocators.get("https://www.demoqa.com/automation-practice-form");
        driverLocators.findElement(By.name("gender"));


        /**
         *
         *
         *
         */

        /**<-- FIND ELEMENT OR FIND ELEMENTS-->
         * 2.- What is the difference between findElement and findElements?
         *
         * findElement: A command used to uniquely identify a web element within the web page.
         *              Returns the first matching web element if multiple web elements are discovered by the locator.
         *              Detects a unique web element.
         * findElements: A command used to identify a list of web elements within the web page.
         *               Returns a list of multiple matching web elements.
         *               Returns a collection of matching elements.
         *
         */

        // Create a new instance of the FireFox driver
        WebDriver driverElements = new ChromeDriver();

        // Launch the Online Store WebSite
        driverElements.get("https://toolsqa.com/Automation-practice-form/");

        // Click on "Partial Link Text" link
        driverElements.findElement(By.partialLinkText("Partial")).click();
        System.out.println("Partial Link Test Pass");

        // Convert element in to a string
        String sClass = driverElements.findElements(By.tagName("button")).toString();
        System.out.println(sClass);

        driverElements.close();

        /**
         *
         *
         *
         */

        /**<-- ABSOLUTE AND RELATIVE XPATH-->
         * 3.- Difference between absolute and relative XPATH?
         *
         * With Absolute Xpath we select the element from the root <html> and cover the whole path to the element.
         * It is also known as complete or Full Xpath.
         * In Relative Xpath expression can starts in the middle of the HTML DOM structure.
         *
         */

        WebDriver driverXpath = new ChromeDriver();

        driverXpath.get("https://demoqa.com/text-box");

        //Absolute
        // Single slash “/” to validate image at start of page
        Boolean imgFlag = driverXpath.findElement(By.xpath("/html/body/div/header/a/img")).isDisplayed();
        System.out.println("The image is displayed : " + imgFlag);

        //Relative
        // Double slash “//” to validate image
        Boolean img_Flag = driverXpath.findElement(By.xpath("//img")).isDisplayed();
        System.out.println("The image is displayed (located by //) : " + img_Flag);
        
        driverXpath.close();

        /**
         *
         *
         *
         */

        /**<-- AXES -->
         * 4.- What is xPath-axes tag?
         *
         * XPath axes are those axes that are used to search for the multiple nodes in the XML document from the current 
         * node context.
         * As location path defines the location of a node using absolute or relative path, axes are used to identify elements 
         * by their relationship like parent, child, sibling, etc.
         * Axes are named so because they refer to axis on which elements are lying relative to an element.
         * These methods are mainly used when the web element is not identified with the help of ID, name, class name, link 
         * text, CSS selector and XPath, etc. locators.
         *
         */

        //Creating the object for chrome driver
        WebDriver driverAxes = new ChromeDriver();
        //Navigate to Google search page
        driverAxes.get("https://www.google.com");
        driverAxes.manage().window().maximize();
        //Using the parent XPath axes
        driverAxes.findElement(By.xpath("//div[@class='FPdoLcVlcLAe']//input[@name='btnK']/parent::*/parent::*/parent::div[1]//input[@name='q']")).click();
        Thread.sleep(3000);
        System.out.println("Parent axes is clicked");
        
        //Closing the browser
        driverAxes.close();

        /**
         *
         *
         *
         */

        /**<-- DROPDOWN -->
         * 5.- How to select dropdown using Selenium?
         *
         * The Select class of Selenium WebDriver provides the following methods to select an option/value from a drop-down:
         *
         * selectByIndex: This method selects the dropdown option by its index number. We provide an integer value as the index 
         * number as an argument.
         * selectByValue: This method selects the dropdown option by its value. We provide a string value as the value as an 
         * argument.
         * selectByVisibleText: This method enables one to select one option from the dropdown or multi-select dropdown based on 
         * the dropdown text.
         * You need to pass the String value of the <select> element as an argument.
         *
         */

        //Creating instance of Chrome driver
        WebDriver driverDropDown = new ChromeDriver();

        // Launching URL
        driverDropDown.get("https://demoqa.com/select-menu");
        //Maximizing window
        driverDropDown.manage().window().maximize();

        //Selecting the dropdown element by locating its id
        Select select = new Select(driverDropDown.findElement(By.id("oldSelectMenu")));

        //Printing the options of the dropdown
        //Get list of web elements
        List<WebElement> lst = select.getOptions();

        //Looping through the options and printing dropdown options
        System.out.println("The dropdown options are:");
        for(WebElement options: lst)
            System.out.println(options.getText());

        //Selecting the option as 'Purple'-- selectByIndex
        System.out.println("Select the Option by Index 4");
        select.selectByIndex(4);
        System.out.println("Select value is: " + select.getFirstSelectedOption().getText());

        driverDropDown.close();
    }
}
