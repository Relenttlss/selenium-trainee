import java.util.Scanner;

public class CountCharInString {
    /*
    //  Java program to count the letters, spaces, numbers and
    //  other characters of an input string
    */

    private static Scanner sc;
    public static void main(String[] args) {
        String str;
        /*
        //Scanner to get input from user
        */

        sc= new Scanner(System.in);
        System.out.println("Please Enter String");
        str = sc.nextLine();

        //  Call the function count to print the result
        CountDigitsAlphabetsSpecials(str);
    }

    /*
    //  Function that return prints which is the count of times the characters
    //  are found in the string
    */

    public static void CountDigitsAlphabetsSpecials(String str) {
        char ch;
        int alph, digit, spl, spaces;
        alph = digit = spl = spaces = 0;

        for(int i = 0; i < str.length(); i++)
        {
            ch = str.charAt(i);
            if(Character.isDigit(ch)) {
                digit++;
            }
            else if(Character.isAlphabetic(ch))  {
                alph++;
            }
            else if(Character.isWhitespace(ch)){
                spaces++;
            }
            else {
                spl++;
            }
        }

        //Final print
        System.out.println("\nNumber of Alphabet Characters = " + alph);
        System.out.println("Number of Numerical Characters = " + digit);
        System.out.println("Number of Whitespace = " + spaces);
        System.out.println("Number of Special Characters = " + spl);
    }
}
