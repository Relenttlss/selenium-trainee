import java.util.Arrays;
import java.util.Scanner;

public class LargestElements {
    /*
    //  Java program to find the 3 largest elements in a given array
    */
    private static Scanner scanner;

    public static void main(String[] args) {
        /*
        //Scanner to get input from user
        */
        scanner = new Scanner(System.in);
        System.out.println("Please Enter array size");

        int columns = scanner.nextInt();
        int[] array = new int[columns];

        for (int i = 0; i < columns; i++) {
            System.out.println("Please Enter Position "+ i+" Value");
            int num = scanner.nextInt();
            array[i] = num;
        }
        scanner.close();

        System.out.println("Original Array:");
        System.out.println(Arrays.toString(array));
        System.out.println("Three largest elements of the given array are:");
        find3largest(array);
    }

    /*
    //   Function called to sort the array and check for the
    //   3 largest int given in the array
     */
    public static void find3largest(int[] arr) {
        Arrays.sort(arr);

        int n = arr.length;
        int check = 0, count = 1;
        for (int i = 1; i <= n; i++) {
           if (count < 4) {
               // This if is used to check and handle for duplicate values
               if (check != arr[n - i]) {
                   System.out.print(arr[n - i] + " ");
                   check = arr[n - i];
                   count++;
               }
               }
               else
                   break;
            }
        }
}
