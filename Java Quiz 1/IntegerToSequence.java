import java.util.LinkedList;
import java.util.Scanner;

public class IntegerToSequence {
    /*
    //  Java program to break an integer into
    //  a sequence of individual digits
     */
    public static void main(String[] args) {
        /*
        //  Scanner to get the int to split from the user
        */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter any number");
        int number = scanner.nextInt();
        scanner.close();

        //List to stack the no. so they can be shown in order
        LinkedList<Integer> stack = new LinkedList<Integer>();

        /*
        //  While loop to count the digits in the input
        //  and push them into a stack
         */
        while (number > 0) {
            stack.push( number % 10 );
            number = number / 10;
        }

        /*
        //  While loop to print the digits in order
        //  by popping them off
         */
        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }
}

